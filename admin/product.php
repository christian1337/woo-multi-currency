<?php

/*
Class Name: WOOMULTI_CURRENCY_F_Admin_Product
Author: Andy Ha (support@villatheme.com)
Author URI: http://villatheme.com
Copyright 2015-2017 villatheme.com. All rights reserved.
*/
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class WOOMULTI_CURRENCY_F_Admin_Product {
	protected $settings;

	function __construct() {

		$this->settings = new WOOMULTI_CURRENCY_F_Data();
		if ( $this->settings->check_fixed_price() ) {
			/*Simple product*/
			add_action( 'woocommerce_product_options_pricing', array( $this, 'simple_price_input' ) );
			/*Variable product*/
			add_action( 'woocommerce_variation_options_pricing', array( $this, 'variation_price_input' ), 10, 3 );
			/*Save data*/
			add_action( 'woocommerce_process_product_meta_simple', array( $this, 'save_meta_simple_product' ) );
			add_action( 'woocommerce_process_product_meta_external', array( $this, 'save_meta_simple_product' ) );

			add_action( 'woocommerce_save_product_variation', array( $this, 'save_meta_product_variation' ), 10, 2 );

			add_action( 'woocommerce_variable_product_bulk_edit_actions', array( $this, 'bulk_edit_actions' ) );

		}
	}

	public function bulk_edit_actions() {
		$currencies = $this->settings->get_currencies();  ?>
		<optgroup label="<?php esc_attr_e( 'Multi Currency', 'woo-multi-currency' ); ?>">
			<?php if ( count( $currencies ) ) {
				foreach ( $currencies as $currency ) {
					if ( $currency == $this->settings->get_default_currency() ) {
						continue;
					}
					?>
					<option value="wbs_regular_price-<?php echo esc_attr( $currency ) ?>">
						<?php echo esc_html__( 'Set regular prices', 'woo-multi-currency' ) . ' (' . $currency . ')'; ?>
					</option>
					<option value="wbs_sale_price-<?php echo esc_attr( $currency ) ?>">
						<?php echo esc_html__( 'Set sale prices', 'woo-multi-currency' ) . ' (' . $currency . ')'; ?>
					</option>
				<?php }
			} ?>
		</optgroup>
	<?php
	}

	/**
	 * Add Regular price, Sale price with Simple product
	 * Working with currency by country
	 */
	public function simple_price_input() {

		global $post;
		$currencies = $this->settings->get_currencies();
		foreach ( $currencies as $currency ) {
			if ( $currency != $this->settings->get_default_currency() ) { ?>
				<div style="border-left: 5px solid #f78080;">
					<p class="form-field ">
						<label for="_regular_price_<?php esc_attr_e( $currency ); ?>">
							<?php echo __( 'Regular Price', 'woo-multi-currency' ) . ' (' . $currency . ')'; ?>
						</label>
						<?php $regular_price = get_post_meta( $post->ID, '_regular_price_' . $currency, true ) ?>
						<input id="_regular_price_<?php esc_attr_e( $currency ); ?>"
							class="short wc_input_price" type="text"
							value="<?php $regular_price ? esc_attr_e( $regular_price ) : esc_attr_e( '' ); ?>"
							name="<?php echo '_regular_price_' . $currency ?>">
					</p>
					<p class="form-field ">
						<label for="_sale_price_<?php esc_attr_e( $currency ); ?>">
							<?php echo __( 'Sale Price', 'woo-multi-currency' ) . ' (' . $currency . ')'; ?>
						</label>
						<?php $sale_price = get_post_meta( $post->ID, '_sale_price_' . $currency, true ) ?>
						<input id="_sale_price_<?php esc_attr_e( $currency ); ?>"
							class="short wc_input_price" type="text"
							value="<?php $sale_price ? esc_attr_e( $sale_price) : esc_attr_e( '' ); ?>"
							name="<?php echo '_sale_price_' . $currency ?>">
					</p>
				</div>
				<?php
			}
		}
		wp_nonce_field( 'wmc_save_simple_product_currency', '_wmc_nonce' );

	}

	/**
	 * Add Regular price, Sale price with Variation product
	 * Working with currency by country
	 *
	 * @param $loop
	 * @param $variation_data
	 * @param $variation
	 */
	public function variation_price_input( $loop, $variation_data, $variation ) {
		$selected_currencies = $this->settings->get_currencies();
		foreach ( $selected_currencies as $code ) {
			$_regular_price = get_post_meta( $variation->ID, '_regular_price_' . $code, true );
			$_sale_price = get_post_meta( $variation->ID, '_sale_price_' . $code, true );
			if ( $code != $this->settings->get_default_currency() ) {
				?>
				<div>
					<p class="form-row form-row-first">
						<label>
							<?php echo esc_html__( 'Regular Price:', 'woo-multi-currency' ) . ' (' . $code . ')'; ?>
						</label>
						<input
							type="text"
							size="5"
							name="<?php echo '_regular_price_' . $code ?>"
							value="<?php echo ( isset( $_regular_price ) ) ? esc_attr( $_regular_price ) : '' ?>"
							class="wc_input_price  wbs-variable-regular-price-<?php echo esc_attr( $code ) ?>" />
					</p>
					<p class="form-row form-row-last">
						<label>
							<?php echo esc_html__( 'Sale Price:', 'woo-multi-currency' ) . ' (' . $code . ')'; ?>
						</label>
						<input
							type="text"
							size="5"
							name="<?php echo '_sale_price_' . $code ?>"
							value="<?php echo ( isset( $_sale_price ) ) ? esc_attr( $_sale_price ) : '' ?>"
							class="wc_input_price wbs-variable-sale-price-<?php echo esc_attr( $code ) ?>" />
					</p>
				</div>
				<?php
			}
		}
		wp_nonce_field( 'wmc_save_variable_product_currency', '_wmc_nonce' );

	}

	/**
	 * Save Price by country of Simple Product
	 *
	 * @param $post_id
	 */
	public function save_meta_simple_product( $post_id ) {
		/*Check Permission*/
		if ( ! current_user_can( 'manage_options' ) ) {
			return;
		}
		/*Check send from product edit page*/
		if ( ! isset( $_POST['_wmc_nonce'] ) || ! wp_verify_nonce( $_POST['_wmc_nonce'], 'wmc_save_simple_product_currency' ) ) {
			return;
		}

		$selected_currencies = $this->settings->get_currencies();
		foreach ( $selected_currencies as $code ) {
			$regular_price_string = '_regular_price_' . $code;
			$sale_price_string = '_sale_price_' . $code;
			if ( isset( $_POST[$regular_price_string] ) ) {
				update_post_meta( $post_id, $regular_price_string, wc_clean( $_POST[$regular_price_string] ) );
			}
			if ( isset( $_POST[$sale_price_string] ) ) {
				update_post_meta( $post_id, $sale_price_string, wc_clean( $_POST[$sale_price_string] ) );
			} else {
				update_post_meta( $post_id, $sale_price_string, '' );
			}

			$date_to = isset( $_POST['_sale_price_dates_to' . $code] )
				? wc_clean( $_POST['_sale_price_dates_to' . $code] ) : '';

			if ( $date_to && strtotime( $date_to ) < strtotime( 'NOW', current_time( 'timestamp' ) ) ) {
				update_post_meta( $post_id, '_sale_price' . $code, '' );
			}
		}
	}

	/**
	 * Save Currency by Country of Variation product
	 *
	 * @param $variation_id
	 * @param $i
	 */
	public function save_meta_product_variation( $variation_id, $i ) {
		/*Check Permission*/
		if ( ! current_user_can( 'manage_options' ) ) {
			return;
		}
		/*Check send from product edit page*/
		if ( ! isset( $_POST['_wmc_nonce'] ) || ! wp_verify_nonce( $_POST['_wmc_nonce'], 'wmc_save_variable_product_currency' ) ) {
			return;
		}
		$selected_currencies = $this->settings->get_currencies();
		foreach ( $selected_currencies as $code ) {
			$regular_price_string = '_regular_price_' . $code;
			$sale_price_string = '_sale_price_' . $code;
			if ( isset( $_POST[$regular_price_string] ) ) {
				update_post_meta( $variation_id, $regular_price_string, wc_clean( $_POST[$regular_price_string] ) );
			}
			if ( isset( $_POST[$sale_price_string] ) ) {
				update_post_meta( $variation_id, $sale_price_string, wc_clean( $_POST[$sale_price_string] ) );
			} else {
				update_post_meta( $variation_id, $sale_price_string, '' );
			}

			$date_to = isset( $_POST['_sale_price_dates_to' . $code] )
				? wc_clean( $_POST['_sale_price_dates_to' . $code] ) : '';

			if ( $date_to && strtotime( $date_to ) < strtotime( 'NOW', current_time( 'timestamp' ) ) ) {
				update_post_meta( $variation_id, '_sale_price' . $code, '' );
			}
		}

	}


} ?>